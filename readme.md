CloudFlare IP Updater
=====================

This system was developed to allow me to automatically update the ip address
from a non-static-ip location.

Requirements
------------

1.  Python 3+

2.  requests library

Sample Config
-------------

The following is a sample entry for a single subdomain

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
[sub.mydomain.com]
api = .......
domain = mydomain.com
email = my-cloudflare-login-email-address@gmail.com
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The first line contains the subdomain you wish to affect, it must be contained
by square brackets [ ] as well as include the domain name.

The second line is the API key, this needs to be included with every domain
added because the API key could be different depending on accounts.

The third line is the root domain name, when you first login to CloudFlare and
are presented with a table listing all domains on your account, that name is
what you need to supply here.

The fourth line is the email address you use to login to cloudflare to access
this account.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
[mydomain.com]
.....

[www.mydomain.com]
.....
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The above sections are two different entries within CloudFlare.
