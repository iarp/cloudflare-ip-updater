import cloudflare
import requests
import functions
import sys
import traceback
from datetime import datetime
import logsystem
import logging
import glob

logsystem.LogSystem(logging.DEBUG)
log = logging.getLogger('system')

external_ip = ''
data = {}
try:
    request = requests.get('http://www.jsonip.com')
    data = request.json()
    external_ip = data['ip']
except:
    log.debug(traceback.format_exc())

if not external_ip:
    log.critical('No IP returned. Failure.')

    try:
        log.debug(request.text)
    except ValueError:
        pass

    sys.exit()

for file in glob.glob('F:/Projects/cloudflare api/*.ini'):

    config = functions.config_load(file)

    for site, data in config.items():
        cf = cloudflare.CloudFlare(data['email'], data['domain'], data['api'])

        if data.get('disabled', False):
            print('Site {} is disabled'.format(site))
            continue

        update_ip = external_ip
        if 'ip' in data:
            update_ip = data['ip']

        if external_ip == data.get('last_ip', None):
            print('last_ip matched in config, skipping {} {}'.format(site, update_ip))
            continue

        try:
            cf.get_domain_dns_records()

            if cf.records[site]['content'] != update_ip:
                log.info('Updating {} from {} to {}'.format(site, cf.records[site]['content'], update_ip))
                cf.change_ip(site, update_ip)

                config[site]['last_ip'] = update_ip
                config[site]['last_updated'] = datetime.now()
            else:
                log.info('CloudFlare IP already matches, skipping {} {}'.format(site, update_ip))

        except KeyError:
            log.info('KeyError on content, good chance domain no longer exists in CF. Disabling')
            config[site]['disabled'] = True
            log.debug(traceback.format_exc())
        except:
            log.debug(traceback.format_exc())

    functions.config_save(file, config)
