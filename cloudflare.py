import requests


class CloudFlare:

    url = 'https://www.cloudflare.com/api_json.html'
    email = ''
    token = ''
    domain = ''

    records = {}

    def __init__(self, email, domain, token):
        self.email = email
        self.domain = domain
        self.token = token

    def get_domain_dns_records(self):

        args = {
            'a': 'rec_load_all',
            'tkn': self.token,
            'email': self.email,
            'z': self.domain
        }
        data = requests.post(self.url, args).json()

        dns = data['response']['recs']['objs']

        self.records = {}
        for item in dns:

            if item['type'] != 'A':
                continue

            self.records[item['name']] = {
                'name': item['name'],
                'content': item['content'],
                'id': item['rec_id'],
                'type': item['type'],
                'service_mode': item['service_mode'],
                'ttl': item['ttl']
            }
        return self.records

    def change_ip(self, domain, ip_address):
        args = {
            'a': 'rec_edit',
            'tkn': self.token,
            'email': self.email,
            'z': self.domain,
            'id': self.records[domain]['id'],
            'type': self.records[domain]['type'],
            'name': self.records[domain]['name'],
            'content': ip_address,
            'service_mode': self.records[domain]['service_mode'],
            'ttl': self.records[domain]['ttl']
        }
        requests.post(self.url, args)

