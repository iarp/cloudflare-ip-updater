from cx_Freeze import setup, Executable
import sys

include_files = ['config.ini', 'cacert.pem']
includes = []
excludes = []
packages = ['decimal']

base = None
if sys.platform == "win32":
    base = "Win32GUI"

setup(
    name='ClourdFlare dynamic ip updater.',
    version='1.0',
    description='',
    author='.',
    author_email='',
    options={'build_exe': {
        'excludes': excludes,
        'packages': packages,
        'include_files': include_files
    }},
    executables=[
        Executable('updater.py'),
        ]
)
