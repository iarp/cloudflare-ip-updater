import os
import json
import sys
import datetime
from collections import OrderedDict


try:
    import configparser as cfgp
except ImportError:
    import ConfigParser as cfgp


def config_load(file_location='config.ini'):
    """
    Loads configuration ini files.
    :type file_location: basestring
    """
    config = OrderedDict()
    cp = cfgp.ConfigParser()
    cp.optionxform = str

    file_path = os.path.dirname(os.path.abspath(sys.argv[0]))
    file_location = os.path.join(file_path, file_location)
    cp.read(file_location)
    for sec in cp.sections():
        if sec not in config:
            config[sec] = {}
        for opt in cp.options(sec):
            if opt not in config[sec]:
                config[sec][opt] = {}
            data = cp.get(sec, opt)

            if data.lower() == 'true':
                data = True
            elif data.lower() == 'false':
                data = False

            try:
                data = json.loads(data)
            except (ValueError, TypeError):
                pass

            # Attempt to load a datetime stamp
            try:
                data = datetime.datetime.strptime(data, '%Y-%m-%d %H:%M:%S')
            except ValueError:
                pass

            config[sec][opt] = data

    return config


def config_save(file_location, config, json_values=None):
    """
    Saves the configuration ini file.
    :type file_location: basestring
    :type config: dict
    """

    cp = cfgp.ConfigParser()
    cp.optionxform = str
    for folder_name in config:
        cp.add_section(folder_name)
        for field in config[folder_name]:
            field_value = config[folder_name][field]

            if isinstance(field_value, dict) or isinstance(field_value, list):
                try:
                    field_value = json.dumps(field_value)
                except ValueError:
                    pass
            elif isinstance(field_value, datetime.datetime):
                field_value = field_value.strftime('%Y-%m-%d %H:%M:%S')
            elif field_value is True:
                field_value = 'True'
            elif field_value is False:
                field_value = 'False'

            cp.set(folder_name, field, field_value)

    file_path = os.path.dirname(os.path.abspath(sys.argv[0]))
    file_location = os.path.join(file_path, file_location)

    with open(file_location, 'w') as cfg:
        cp.write(cfg)

    return config
