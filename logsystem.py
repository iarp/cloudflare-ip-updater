import logging
import logging.handlers
import os
import sys


class LogSystem:

    def __init__(self, level=logging.INFO):

        file_path = os.path.dirname(sys.argv[0])
        log_path = os.path.join(file_path, 'logs')
        print('Logging to ', log_path)

        if not os.path.isdir(log_path):
            os.mkdir(log_path)

        self.setup_logs('system', '{}/system.log'.format(log_path), level)

    @staticmethod
    def setup_logs(logger_name, log_file, level):
        l = logging.getLogger(logger_name)

        file_formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
        file_handler = logging.handlers.RotatingFileHandler(log_file, mode='a', maxBytes=10485760, backupCount=3)
        # file_handler = logging.FileHandler(log_file, mode='a')
        file_handler.setFormatter(file_formatter)

        stream_formatter = logging.Formatter('%(name)s - %(asctime)s - %(levelname)s - %(message)s')
        stream_handler = logging.StreamHandler()
        stream_handler.setFormatter(stream_formatter)

        l.setLevel(level)
        l.addHandler(file_handler)
        l.addHandler(stream_handler)

        return logging.getLogger(logger_name)
